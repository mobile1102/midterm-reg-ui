import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8);
  }

  List<String> menu = [
    "ลงทะเบียน",
    "ผลการลงทะเบียน",
    "ผลอนุมัติเพิ่ม-ลด",
    "ประวัตินิสิต",
    "ภาระค่าใช้จ่ายทุน",
    "ตรวจสอบจบ",
    "ยื่นคำร้อง",
    "เสนอความคิดเห็น",
    "รายชื่อนิสิต",
    "ประวัติการเข้าใช้ระบบ",
    "ตรวจหนี้สิน(นิสิตที่ยังไม่จบ)",
  ];

  List<String> card = [
    "https://www.i-pic.info/i/UN3B354791.jpg",
    "https://www.i-pic.info/i/nzrE354797.jpg",
    "https://www.i-pic.info/i/jZFr355798.png",
    "https://www.i-pic.info/i/CJfL355799.png",
  ];

  int current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      // appBar: AppBar(
      //   backgroundColor: Colors.white,
      //   elevation: 0.0,
      //   title: Image(
      //     image: NetworkImage(
      //       'https://www.i-pic.info/i/SEAI352024.png',
      //       scale: 2.0,
      //     ),
      //   ),
      //   leading: IconButton(
      //     padding: EdgeInsets.only(left: 30.0),
      //     onPressed: () => print('Menu'),
      //     icon: Icon(Icons.menu),
      //     iconSize: 30.0,
      //     color: Colors.black,
      //   ),
      //   actions: <Widget>[
      //     IconButton(
      //       padding: EdgeInsets.only(right: 30.0),
      //       onPressed: () => print('Logout'),
      //       icon: Icon(Icons.logout),
      //       iconSize: 30.0,
      //       color: Colors.black,
      //     ),
      //   ],
      // ),
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '63160002 คณะวิทยาการสารสนเทศ',
                    ),
                    Text(
                      'ก้องภพ เคนรัง',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    // Text(
                    //   'คณะวิทยาการสารสนเทศ',
                    //   style: TextStyle(fontWeight: FontWeight.bold),
                    // ),
                  ],
                ),
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.yellow[300],
                  backgroundImage: NetworkImage(
                      'https://cdn-icons-png.flaticon.com/512/3135/3135715.png'),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            width: double.infinity,
            height: 36,
            child: ListView.builder(
              shrinkWrap: true,
              physics: const BouncingScrollPhysics(),
              itemCount: menu.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      current = index;
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      left: index == 0 ? 24.0 : 15,
                      right: index == menu.length - 1 ? 24.0 : 0,
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    height: 36,
                    decoration: BoxDecoration(
                        color: current == index
                            ? Color.fromARGB(255, 0, 0, 0)
                            : Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        border: current == index
                            ? null
                            : Border.all(
                                color: Colors.grey,
                                width: 1,
                              )),
                    child: Row(children: [
                      Text(
                        menu[index],
                        style: TextStyle(fontWeight: FontWeight.bold).copyWith(
                          color: current == index ? Colors.white : Colors.black,
                          fontSize: 10,
                        ),
                      )
                    ]),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35, left: 25),
            child: Row(children: <Widget>[
              Text(
                'ประกาศเรื่อง',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ]),
          ),
          Container(
            height: 260,
            child: ListView.builder(
              padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
              // shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 1,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  height: 260,
                  width: 360,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: NetworkImage(
                            'https://www.i-pic.info/i/7Ej0354782.png'),
                            scale: 1,
                        // fit: BoxFit.cover,
                      ),
                      color: Colors.yellow[300],
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x10000000),
                          blurRadius: 10,
                          spreadRadius: 4,
                          offset: Offset(0.0, 8.0),
                        )
                      ]),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 35, left: 25),
            child: Row(children: <Widget>[
              Text(
                'ข่าวสารจาก คณะวิทยาการสารสนเทศ',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ]),
          ),
          Container(
            height: 320,
            child: ListView.builder(
              padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: card.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(right: 20),
                  height: 320,
                  width: 220,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: NetworkImage(
                            card[index]),
                            scale: 1,
                        fit: BoxFit.cover,
                      ),
                      color: Colors.yellow[300],
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x10000000),
                          blurRadius: 10,
                          spreadRadius: 4,
                          offset: Offset(0.0, 8.0),
                        )
                      ]),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
