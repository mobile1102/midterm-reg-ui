import 'package:flutter/material.dart';
import 'package:midterm_reg/grades_screen.dart';
import 'package:midterm_reg/home_screen.dart';
import 'package:device_preview/device_preview.dart';
import 'package:midterm_reg/profile_screen.dart';
import 'package:midterm_reg/schedule_screen.dart';

void main() => runApp(MainPage());

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  // const MyApp({Key? key}) : super(key: key);

  int page = 0;
  final screens = [
    HomeScreen(),
    // Center(child: Text('ตารางเรียน/สอบ', style: TextStyle(fontSize: 48))),
    ScheduleScreen(),
    // Center(child: Text('ผลการศึกษา', style: TextStyle(fontSize: 48))),
    GradesScreen(),
    // Center(child: Text('โปรไฟล์', style: TextStyle(fontSize: 48))),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          useInheritedMediaQuery: true,
          builder: DevicePreview.appBuilder,
          locale: DevicePreview.locale(context),
          title: 'REG BUU by 63160002 Kongphop Kenrung',
          home: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              title: Image(
                image: NetworkImage(
                  'https://www.i-pic.info/i/SEAI352024.png',
                  scale: 2.0,
                ),
              ),
              leading: IconButton(
                padding: EdgeInsets.only(left: 30.0),
                onPressed: () => print('Menu'),
                icon: Icon(Icons.menu),
                iconSize: 30.0,
                color: Colors.black,
              ),
              actions: <Widget>[
                IconButton(
                  padding: EdgeInsets.only(right: 30.0),
                  onPressed: () => print('Logout'),
                  icon: Icon(Icons.logout),
                  iconSize: 30.0,
                  color: Colors.black,
                ),
              ],
            ),
            body: screens[page],
            bottomNavigationBar: NavigationBarTheme(
              data: NavigationBarThemeData(
                  indicatorColor: Colors.yellow[300],
                  labelTextStyle: MaterialStateProperty.all(
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500))),
              child: NavigationBar(
                height: 65,
                backgroundColor: Colors.white,
                labelBehavior:
                    NavigationDestinationLabelBehavior.onlyShowSelected,
                selectedIndex: page,
                onDestinationSelected: (page) =>
                    setState((() => this.page = page)),
                destinations: [
                  NavigationDestination(
                    icon: Icon(Icons.home_outlined),
                    selectedIcon: Icon(Icons.home),
                    label: 'หน้าหลัก',
                  ),
                  NavigationDestination(
                    icon: Icon(Icons.table_chart_outlined),
                    selectedIcon: Icon(Icons.table_chart_rounded),
                    label: 'ตารางเรียน',
                  ),
                  NavigationDestination(
                    icon: Icon(Icons.contact_page_outlined),
                    selectedIcon: Icon(Icons.contact_page),
                    label: 'ผลการศึกษา',
                  ),
                  NavigationDestination(
                    icon: Icon(Icons.account_circle_outlined),
                    selectedIcon: Icon(Icons.account_circle),
                    label: 'โปรไฟล์',
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
