import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8);
  }

  int current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.yellow[300],
                  backgroundImage: NetworkImage(
                      'https://cdn-icons-png.flaticon.com/512/3135/3135715.png'),
                  
                ),
                Text(
                  'ก้องภพ เคนรัง',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  '63160002 คณะวิทยาการสารสนเทศ',
                ),
                Text(
                  'หลักสูตร: 2115020',
                ),
                Text(
                  'วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ',
                ),
                Text(
                  'สถานภาพ: กำลังศึกษา',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  'อ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  'ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
