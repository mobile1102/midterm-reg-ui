import 'package:flutter/material.dart';

class GradesScreen extends StatefulWidget {
  @override
  _GradesScreenState createState() => _GradesScreenState();
}

class _GradesScreenState extends State<GradesScreen> {
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8);
  }

  List<String> card1 = [
    "https://www.i-pic.info/i/2bXp355829.png",
    "https://www.i-pic.info/i/3Myi355830.png",
  ];

  List<String> card2 = [
    "https://www.i-pic.info/i/FIs9355831.png",
    "https://www.i-pic.info/i/bkRF355832.png",
  ];

  List<String> card3 = [
    "https://www.i-pic.info/i/j2OB355833.png",
    "https://www.i-pic.info/i/AbmH355834.png",
  ];

  int current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '63160002 คณะวิทยาการสารสนเทศ',
                    ),
                    Text(
                      'ก้องภพ เคนรัง',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.yellow[300],
                  backgroundImage: NetworkImage(
                      'https://cdn-icons-png.flaticon.com/512/3135/3135715.png'),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, left: 25),
            child: Row(children: <Widget>[
              Text(
                'ชั้นปีที่1',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ]),
          ),
          Container(
            height: 180,
            child: ListView.builder(
              padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
              // shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: card1.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  height: 180,
                  width: 300,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: NetworkImage(card1[index]),
                        scale: 1,
                        // fit: BoxFit.cover,
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x10000000),
                          blurRadius: 10,
                          spreadRadius: 4,
                          offset: Offset(0.0, 8.0),
                        )
                      ]),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, left: 25),
            child: Row(children: <Widget>[
              Text(
                'ชั้นปีที่2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ]),
          ),
          Container(
            height: 180,
            child: ListView.builder(
              padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
              // shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: card2.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  height: 180,
                  width: 300,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: NetworkImage(card2[index]),
                        scale: 1,
                        // fit: BoxFit.cover,
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x10000000),
                          blurRadius: 10,
                          spreadRadius: 4,
                          offset: Offset(0.0, 8.0),
                        )
                      ]),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, left: 25),
            child: Row(children: <Widget>[
              Text(
                'ชั้นปีที่3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ]),
          ),
          Container(
            height: 180,
            child: ListView.builder(
              padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
              // shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: card3.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  height: 180,
                  width: 300,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                        image: NetworkImage(card3[index]),
                        scale: 1,
                        // fit: BoxFit.cover,
                      ),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x10000000),
                          blurRadius: 10,
                          spreadRadius: 4,
                          offset: Offset(0.0, 8.0),
                        )
                      ]),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 25),
            child: Row(children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'การแสดงเกรด',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                        fontSize: 10),
                  ),
                  Text(
                    '1. กรณีที่นิสิตประเมินครบทุกรายวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะเป็นเกรดที่ได้',
                    style: TextStyle(color: Colors.red, fontSize: 10),
                  ),
                  Text(
                    '2. กรณีที่นิสิตประเมินครบทุกวิชาแล้ว แต่เกรดยังไม่ออก ช่องแสดงเกรดจะเป็นช่องว่าง',
                    style: TextStyle(color: Colors.red, fontSize: 10),
                  ),
                  Text(
                    '3. กรณีที่นิสิตประเมินไม่ครบทุกวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะมีเครื่องหมาย ??',
                    style: TextStyle(color: Colors.red, fontSize: 10),
                  ),
                ],
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
